import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from initialise import Initialise
from flask import request
from sqlalchemy import text
from http import HTTPStatus
import sys
sys.path.append("..")
 
app = Flask(__name__)
init = Initialise()
app = init.db(app)
 
db = SQLAlchemy(app)
migrate = Migrate(app, db)
 
class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    surname = db.Column(db.String(128))
    identity_number = db.Column(db.Integer())
 
@app.route('/')
def home():
    return 'This is the home page'

@app.route('/hello')
def hello():
    return 'Hello'

def hateoas(id):
    return [
        {
            "rel": "self",
            "resource": "http://172.26.32.1:8000/v1/users/{id}",
            "method": "GET"
        },
        {
            "rel": "update",
            "resource": "http://172.26.32.1:8000/v1/users/{id}",
            "method": "PATCH"
        },
        {
            "rel": "update",
            "resource": "http://172.26.32.1:8000/v1/users/{id}",
            "method": "DELETE"
        }
    ]
 
@app.route('/v1/users', methods=['POST'])
def post_user_details():
    try:
        data = request.get_json()
        value = create(data)
        return json.dumps(
            {
                "id": value.lastrowid,
                "links": hateoas(value.lastrowid)
            }
        )
    except Exception as e:
        return json.dumps('Failed. ' + str(e)), HTTPStatus.NOT_FOUND
 
def create(post_data):
    sql = text(
        'INSERT INTO users (name, surname, identity_number) values (:name, :surname, :identity_number)'
    )
    return execute(sql, post_data)

def execute(sql, data):
    return db.engine.execute(
        sql,
        data
    )

def return_message(message):
    return json.dumps(message), HTTPStatus.OK
@app.route('/v1/users/<user_id>', methods=['GET'])
def get_user_details(user_id):
    try:
        user = Users.query.get(user_id)
        if user:
            return json.dumps({"name": user.name, "surname": user.surname, "identity_number": user.identity_number, "links": hateoas(user_id)}), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v1/users/<user_id>', methods=['PATCH'])
def patch_user_details(user_id):
    data = request.get_json()
    try:
        user = Users.query.get(user_id)
        if user:
            for key, value in data.items():
                setattr(user, key, value)
            db.session.commit()
            return json.dumps({"id": user.id, "links": hateoas(user_id)}), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v1/users/<user_id>', methods=['DELETE'])
def delete_user_details(user_id):
    try:
        user = Users.query.get(user_id)
        if user:
            db.session.delete(user)
            db.session.commit()
            return json.dumps('Deleted'), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v2/users/<user_id>', methods=['GET'])
def get_user_details_orm(user_id):
    try:
        user = Users.query.get(user_id)
        if user:
            return json.dumps({"name": user.name, "surname": user.surname, "identity_number": user.identity_number, "links": hateoas(user_id)}), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed to retrieve record. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v2/users', methods=['POST'])
def post_user_details_orm():
    try:
        data = request.get_json()
        new_user = Users(name=data['name'], surname=data['surname'], identity_number=data['identity_number'])
        db.session.add(new_user)
        db.session.commit()
        return json.dumps({"id": new_user.id, "links": hateoas(new_user.id)}), HTTPStatus.CREATED
    except Exception as e:
        return json.dumps('Failed to add record. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v2/users/<user_id>', methods=['PATCH'])
def patch_user_details_orm(user_id):
    data = request.get_json()
    try:
        user = Users.query.get(user_id)
        if user:
            for key, value in data.items():
                setattr(user, key, value)
            db.session.commit()
            return json.dumps({"id": user.id, "links": hateoas(user_id)}), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed to update record. ' + str(e)), HTTPStatus.NOT_FOUND
 
@app.route('/v2/users/<user_id>', methods=['DELETE'])
def delete_user_details_orm(user_id):
    try:
        user = Users.query.get(user_id)
        if user:
            db.session.delete(user)
            db.session.commit()
            return json.dumps('Deleted'), HTTPStatus.OK
        else:
            return json.dumps('User not found'), HTTPStatus.NOT_FOUND
    except Exception as e:
        return json.dumps('Failed. ' + str(e)), HTTPStatus.NOT_FOUND
